﻿using CodingChallenge.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.DataAccess.Interface
{
    public interface IBaseRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Obtiene todos los elementos de la lista
        /// </summary>
        /// <param name="navigationProperties">Expresión Linq</param>
        /// <returns></returns>
        IEnumerable<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);

        /// <summary>
        /// Obtiene el listado de elementos en base a los parametros de filtrado usados
        /// </summary>
        /// <param name="where">Expresión Linq Where</param>
        /// <param name="navigationProperties">Expresión Linq</param>
        /// <returns></returns>
        IEnumerable<T> GetList(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);

        /// <summary>
        /// Obtiene un elemento de la lista en base a los paramentros de filtrado usados
        /// </summary>
        /// <param name="where">Expresíón Linq where</param>
        /// <param name="navigationProperties">Expresión Linq</param>
        /// <returns></returns>
        T Get(Func<T, bool> where, params Expression<Func<T, object>>[] navigationProperties);

        /// <summary>
        /// Obtiene un elemento de la lista con base a su identificador
        /// </summary>
        /// <param name="entity">Entidades a buscar</param>
        /// <param name="navigationProperties">Propiedades a buscar</param>
        /// <returns></returns>
        T Get(T entity, params Expression<Func<T, object>>[] navigationProperties);

        /// <summary>
        /// Agrega un listado de elementos
        /// </summary>
        /// <param name="items">Elementos a agregar</param>
        bool Add(params T[] items);

        /// <summary>
        /// Actualiza un listado de elementos
        /// </summary>
        /// <param name="items">Elementos a actualizar</param>
        bool Update(params T[] items);

        /// <summary>
        /// Elimina un listado de elementos
        /// </summary>
        /// <param name="items">Elementos a eliminar</param>
        bool Remove(params T[] items);
    }
}
