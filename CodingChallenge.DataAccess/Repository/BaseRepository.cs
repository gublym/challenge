﻿using CodingChallenge.Common.Exceptions;
using CodingChallenge.DataAccess.Interface;
using CodingChallenge.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.DataAccess.Repository
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseEntity
    {
        /// <summary>
        /// Obtiene todos los elementos de esta entidad
        /// </summary>
        /// <param name="navigationProperties">Propiedades que se van a agregar en los resultados</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                List<T> list;
                using (var context = new challengeEntities())
                {
                    IQueryable<T> dbQuery = context.Set<T>();

                    //Apply eager loading
                    foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                        dbQuery = dbQuery.Include<T, object>(navigationProperty);

                    list = dbQuery
                        .ToList();
                }
                return list;
            }
            catch (Exception ex)
            {
                throw new DataAccessException("BaseRepository<T>", "GetAll", ex);
            }
        }

        /// <summary>
        /// Obtiene un listado de entidades en base a una expressión de consulta Linq
        /// </summary>
        /// <param name="where">Expresión Linq</param>
        /// <param name="navigationProperties">Propiedades que se van a agregar a los resultados</param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetList(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                List<T> list;
                using (var context = new challengeEntities())
                {
                    IQueryable<T> dbQuery = context.Set<T>();

                    //Apply eager loading
                    foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                        dbQuery = dbQuery.Include<T, object>(navigationProperty);

                    list = dbQuery
                        .Where(where)
                        .ToList<T>();
                }
                return list;
            }
            catch (Exception ex)
            {
                throw new DataAccessException("BaseRepository<T>", "GetList", ex);
            }
        }

        /// <summary>
        /// Obtiene un elemento de la entidad en base a una expresión Linq
        /// </summary>
        /// <param name="where">Expresión Linq</param>
        /// <param name="navigationProperties">Propiedades que se van a agregar a la entidad</param>
        /// <returns></returns>
        public virtual T Get(Func<T, bool> where,
             params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                T item = null;
                using (var context = new challengeEntities())
                {
                    IQueryable<T> dbQuery = context.Set<T>();

                    //Apply eager loading
                    foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                        dbQuery = dbQuery.Include<T, object>(navigationProperty);

                    item = dbQuery
                        .Where(where)//Don't track any changes for the selected item
                        .AsQueryable()
                        .FirstOrDefault<T>(); //Apply where clause
                }
                return item;
            }
            catch (Exception ex)
            {
                throw new DataAccessException("BaseRepository<T>", "Get", ex);
            }
        }

        /// <summary>
        /// Obtiene un elemento de la lista con base a su identificador
        /// </summary>
        /// <param name="entity">Entidades a buscar</param>
        /// <param name="navigationProperties">Propiedades a buscar</param>
        /// <returns></returns>
        public virtual T Get(T entity, params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                using (var context = new challengeEntities())
                {
                    T item = null;

                    // finding primary keys
                    var keyExpression = Helpers.EntityKeyHelper.Instance.GetExpressionKeys(entity, context);

                    var data = context.Set<T>();
                    IQueryable<T> dbQuery = data;

                    //Apply eager loading
                    foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                        dbQuery = dbQuery.Include<T, object>(navigationProperty);

                    item = dbQuery
                       .AsNoTracking() //Don't track any changes for the selected item
                       .FirstOrDefault(keyExpression); //Apply key clause
                    return item;
                }
            }
            catch (Exception ex)
            {
                throw new DataAccessException("BaseRepository<T>", "Get", ex);
            }
        }

        /// <summary>
        /// Agrega un listado de entidades
        /// </summary>
        /// <param name="items">Entidades a agregar</param>
        public virtual bool Add(params T[] items)
        {
            try
            {
                using (var context = new challengeEntities())
                {
                    foreach (T item in items)
                    {
                        context.Entry(item).State = EntityState.Added;
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new DataAccessException("BaseRepository<T>", "Add", ex);
            }
        }

        /// <summary>
        /// Actualiza un listado de entidades
        /// </summary>
        /// <param name="items">Entidades a actualizar</param>
        public virtual bool Update(params T[] items)
        {
            try
            {
                using (var context = new challengeEntities())
                {
                    foreach (T item in items)
                    {
                        context.Entry(item).State = EntityState.Modified;
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new DataAccessException("BaseRepository<T>", "Update", ex);
            }
        }

        /// <summary>
        /// Elimina un listado de entidades
        /// </summary>
        /// <param name="items">Entidades a eliminar</param>
        public virtual bool Remove(params T[] items)
        {
            try
            {
                using (var context = new challengeEntities())
                {
                    foreach (T item in items)
                    {
                        context.Entry(item).State = EntityState.Deleted;
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new DataAccessException("BaseRepository<T>", "Remove", ex);
            }
        }
    }
}
