USE [master]
GO
/****** Object:  Database [challenge]    Script Date: 9/10/2020 3:33:02 PM ******/
CREATE DATABASE [challenge]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'challenge', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\challenge.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'challenge_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\challenge_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [challenge] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [challenge].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [challenge] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [challenge] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [challenge] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [challenge] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [challenge] SET ARITHABORT OFF 
GO
ALTER DATABASE [challenge] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [challenge] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [challenge] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [challenge] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [challenge] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [challenge] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [challenge] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [challenge] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [challenge] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [challenge] SET  DISABLE_BROKER 
GO
ALTER DATABASE [challenge] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [challenge] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [challenge] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [challenge] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [challenge] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [challenge] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [challenge] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [challenge] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [challenge] SET  MULTI_USER 
GO
ALTER DATABASE [challenge] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [challenge] SET DB_CHAINING OFF 
GO
ALTER DATABASE [challenge] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [challenge] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [challenge] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [challenge] SET QUERY_STORE = OFF
GO
USE [challenge]
GO
/****** Object:  Table [dbo].[Permissions]    Script Date: 9/10/2020 3:33:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permissions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](150) NULL,
 CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 9/10/2020 3:33:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
	[Description] [varchar](150) NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RolesPermissions]    Script Date: 9/10/2020 3:33:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolesPermissions](
	[IdRol] [int] NOT NULL,
	[IdPermission] [int] NOT NULL,
	[Visible] [bit] NULL,
 CONSTRAINT [PK_RolesPermissions] PRIMARY KEY CLUSTERED 
(
	[IdRol] ASC,
	[IdPermission] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 9/10/2020 3:33:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](50) NULL,
	[Password] [varchar](50) NULL,
	[IdRole] [int] NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Permissions] ON 

INSERT [dbo].[Permissions] ([Id], [Name], [Description]) VALUES (1, N'AdminUser', N'Manage Users')
INSERT [dbo].[Permissions] ([Id], [Name], [Description]) VALUES (2, N'AdminRole', N'Manage Roles')
INSERT [dbo].[Permissions] ([Id], [Name], [Description]) VALUES (3, N'Dashboard', N'Can see Dashboard')
SET IDENTITY_INSERT [dbo].[Permissions] OFF
SET IDENTITY_INSERT [dbo].[Roles] ON 

INSERT [dbo].[Roles] ([Id], [Name], [Description]) VALUES (1, N'Administrator', N'Is the administrator of the system')
INSERT [dbo].[Roles] ([Id], [Name], [Description]) VALUES (2, N'User', N'Is a simple user of the system')
SET IDENTITY_INSERT [dbo].[Roles] OFF
INSERT [dbo].[RolesPermissions] ([IdRol], [IdPermission], [Visible]) VALUES (1, 1, 1)
INSERT [dbo].[RolesPermissions] ([IdRol], [IdPermission], [Visible]) VALUES (1, 2, 1)
INSERT [dbo].[RolesPermissions] ([IdRol], [IdPermission], [Visible]) VALUES (1, 3, 1)
INSERT [dbo].[RolesPermissions] ([IdRol], [IdPermission], [Visible]) VALUES (2, 3, 1)
SET IDENTITY_INSERT [dbo].[Users] ON 

INSERT [dbo].[Users] ([Id], [UserName], [Password], [IdRole]) VALUES (1, N'admin', N'101010', 1)
INSERT [dbo].[Users] ([Id], [UserName], [Password], [IdRole]) VALUES (2, N'pablo.carrera', N'121212', 2)
SET IDENTITY_INSERT [dbo].[Users] OFF
ALTER TABLE [dbo].[RolesPermissions]  WITH CHECK ADD  CONSTRAINT [FK_RolesPermissions_Permissions] FOREIGN KEY([IdPermission])
REFERENCES [dbo].[Permissions] ([Id])
GO
ALTER TABLE [dbo].[RolesPermissions] CHECK CONSTRAINT [FK_RolesPermissions_Permissions]
GO
ALTER TABLE [dbo].[RolesPermissions]  WITH CHECK ADD  CONSTRAINT [FK_RolesPermissions_Roles] FOREIGN KEY([IdRol])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[RolesPermissions] CHECK CONSTRAINT [FK_RolesPermissions_Roles]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Roles] FOREIGN KEY([IdRole])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Roles]
GO
USE [master]
GO
ALTER DATABASE [challenge] SET  READ_WRITE 
GO
