﻿using CodingChallenge.Common.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Common.Exceptions
{
    /// <summary>
    /// Representa una excepción en la capa de servicio
    /// </summary>
    public class ServiceException : BaseException
    {
        /// <summary>
        /// Inicializa una instancia de la clase <see cref="ServiceException"/>
        /// </summary>
        /// <param name="pClass">Clase que manda el error</param>
        /// <param name="pFunction">Método que manda el error</param>
        /// <param name="pInnerException">Excepción Interna</param>
        public ServiceException(string pClass, string pFunction, System.Exception pInnerException)
            : base("Se presentó un error en la capa de Servicio. Revise la excepción Interna.",
                 pClass, pFunction, pInnerException)
        {
            this.Layer = LayerApplication.Service;
        }
    }
}
