﻿using CodingChallenge.Common.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Common.Exceptions
{
    /// <summary>
    /// Representa una excepción en la capa de datos
    /// </summary>
    public class DataAccessException : BaseException
    {
        /// <summary>
        /// Inicializa una instancia de la clase <see cref="DataAccessException"/>
        /// </summary>
        /// <param name="pClass">Clase que manda el error</param>
        /// <param name="pFunction">Método que manda el error</param>
        /// <param name="pInnerException">Excepción Interna</param>
        public DataAccessException(string pClass, string pFunction, System.Exception pInnerException)
            : base("Se presentó un error en la capa de Acceso a Datos. Revise la excepción Interna.",
                 pClass, pFunction, pInnerException)
        {
            this.Layer = LayerApplication.DataAccess;
        }
    }
}
