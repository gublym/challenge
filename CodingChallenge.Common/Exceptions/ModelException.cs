﻿using CodingChallenge.Common.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Common.Exceptions
{
    /// <summary>
    /// Representa una excepción en la capa de Modelo
    /// </summary>
    public class ModelException : BaseException
    {
        /// <summary>
        /// Inicializa una instancia de la clase <see cref="ModelException"/>
        /// </summary>
        /// <param name="pClass">Clase que manda el error</param>
        /// <param name="pFunction">Método que manda el error</param>
        /// <param name="pInnerException">Excepción Interna</param>
        public ModelException(string pClass, string pFunction, System.Exception pInnerException)
            : base("Se presentó un error en la capa de Modelo. Revise la excepción Interna.",
                 pClass, pFunction, pInnerException)
        {
            this.Layer = LayerApplication.Model;
        }
    }
}
