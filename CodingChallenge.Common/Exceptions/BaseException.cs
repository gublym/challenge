﻿using CodingChallenge.Common.Enumerators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Common.Exceptions
{
    /// <summary>
    /// Clase base para el manejo de las excepciones en la aplicación
    /// </summary>
    public class BaseException : System.Exception
    {
        /// <summary>
        /// Inicializa una instancia de la clase <see cref="BaseException"/>
        /// </summary>
        /// <param name="pMessage">Mensaje del error</param>
        /// <param name="pClass">Clase que manda el error</param>
        /// <param name="pFunction">Método que manda el error</param>
        /// <param name="pInnerException">Excepción Interna</param>
        public BaseException(string pMessage, string pClass, string pFunction, System.Exception pInnerException)
            : base(pMessage, pInnerException)
        {
            this.Class = pClass;
            this.Function = pFunction;
        }

        /// <summary>
        /// Obtiene o establece la capa en la que se presenta la excepción
        /// </summary>
        public LayerApplication Layer { get; set; }

        /// <summary>
        /// Obtiene o establece la clase en la que se presenta la excepción
        /// </summary>
        public string Class { get; set; }

        /// <summary>
        /// Obtiene o establece el método en el que se presenta la excepción
        /// </summary>
        public string Function { get; set; }
    }
}
