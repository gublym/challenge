﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Common.Enumerators
{
    /// <summary>
    /// Representa las capas de la aplicacion
    /// </summary>
    public enum LayerApplication
    {
        Model = 0,
        DataAccess = 1,
        Business = 2,
        Service = 3,
        Presentation = 4,
        Common = 5
    }
}
