﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Helpers;
using Newtonsoft.Json;
using System.Net;
using CodingChallenge.Entity;
using CodingChallenge.Business.Entity;
using CodingChallenge.Common.Exceptions;

namespace CodingChallenge.API.Controllers.Base
{
    /// <summary>
    /// Clase base para todos los controllers
    /// </summary>
    /// <typeparam name="T">Objeto de tipo Entidad</typeparam>
    [Authorize]
    public abstract class BaseController<T> : BaseApiController, IBaseController<T> where T : BaseEntity
    {
        /// <summary>
        /// Constructor por defecto
        /// </summary>
        /// <param name="pBusiness"></param>
        public BaseController(BaseBusiness<T> pBusiness)
        {
            _baseBusiness = pBusiness;
        }

        /// <summary>
        /// Propiedad de tipo negocio
        /// </summary>
        protected BaseBusiness<T> _baseBusiness { get; private set; }

        /// <summary>
        /// Servicio que obtiene todos los elementos de este objeto
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public virtual JsonActionResult GetAll()
        {
            try
            {
                var entities = _baseBusiness.GetAll();
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", entities);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("BaseController<T>", "GetAll", ex));
            }
        }

        /// <summary>
        /// Servicio que obtiene el elemento que coincida con el objeto pasado como parametro
        /// </summary>
        /// <param name="pEntity">Entidad a buscar</param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonActionResult GetByEntity([FromBody] T pEntity)
        {
            try
            {
                var entity = _baseBusiness.GetByEntity(pEntity);
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", pEntity);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("BaseController<T>", "GetByEntity", ex));
            }
        }

        /// <summary>
        /// Servicio que actualiza el listado de objetos pasados como parametro
        /// </summary>
        /// <param name="pEntities">Entidades a actualizar</param>
        /// <returns></returns>
        [HttpPut]
        public virtual JsonActionResult Update([FromBody] T pEntity)
        {
            try
            {
                bool result = _baseBusiness.Update(pEntity);
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", result);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("BaseController<T>", "Update", ex));
            }
        }

        /// <summary>
        /// Servicio que agrega el listado de objetos pasados como parametro
        /// </summary>
        /// <param name="pEntities">Entidades a agregar</param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonActionResult Add([FromBody] T pEntity)
        {
            try
            {
                bool result = _baseBusiness.Add(pEntity);
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", result);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("BaseController<T>", "Add", ex));
            }
        }

        /// <summary>
        /// Servicio que elimina el listado de objetos pasados como parametro
        /// </summary>
        /// <param name="pEntities">Entidades a eliminar</param>
        /// <returns></returns>
        [HttpPost]
        public virtual JsonActionResult Delete([FromBody] T pEntity)
        {
            try
            {
                bool result = _baseBusiness.Remove(pEntity);
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", result);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("BaseController<T>", "Delete", ex));
            }
        }

        #region JsonResult methods

        /// <summary>
        /// Devuelve una respuesta OK a la peticion
        /// </summary>
        protected internal virtual JsonActionResult JsonApiResult()
        {
            return new JsonActionResult(Request);
        }

        /// <summary>
        /// Devuelve una respuesta con el codigo indicado y el objeto resultado
        /// </summary>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="Result">Objeto resultado de la transacción</param>
        protected internal virtual JsonActionResult JsonApiResult(HttpStatusCode statusCode, object result)
        {
            return new JsonActionResult(Request, statusCode, result);
        }

        /// <summary>
        /// Devuelve una respuesta con el codigo indicado y el mensaje de la transaccion
        /// </summary>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="StatusDescription">Descripcion de transacción</param>
        protected internal virtual JsonActionResult JsonApiResult(HttpStatusCode statusCode, string statusDescription)
        {
            return new JsonActionResult(Request, statusCode, statusDescription);
        }

        /// <summary>
        /// Devuelve una respuesta con el codigo, el mensaje y el objeto resultado de la transacción
        /// </summary>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="StatusDescription">Descripcion de transacción</param>
        /// <param name="Result">Objeto resultado de la transacción</param>
        protected internal virtual JsonActionResult JsonApiResult(HttpStatusCode statusCode, string statusDescription, object result)
        {
            return new JsonActionResult(Request, statusCode, statusDescription, result);
        }

        #endregion
    }
}
