﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace CodingChallenge.API.Controllers.Base
{
    /// <summary>
    /// Clase que convierte el resultado del metodo del controller en un objeto tipo Json
    /// </summary>
    public class JsonActionResult : IHttpActionResult
    {
        /// <summary>
        /// El HttpResponseMessage del ApiController actual
        /// </summary>
        public HttpRequestMessage Request { get; }

        /// <summary>
        /// Objeto con el resultado de la respuesta
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// Codigo de estado de la respuesta
        /// </summary>
        public HttpStatusCode StatusCode { get; private set; }

        /// <summary>
        /// Mensaje de la respuesta
        /// </summary>
        public string StatusDescription { get; private set; }

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        /// <param name="request">HttpResponseMessage del ApiController actual</param>
        public JsonActionResult(HttpRequestMessage request)
        {
            this.Request = request;
            this.StatusCode = HttpStatusCode.OK;
        }

        /// <summary>
        /// Inicializa el codigo y el resultado de la transacción
        /// </summary>
        /// <param name="request">HttpRequestMessage del ApiController actual</param>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="Result">Objeto resultado de la transacción</param>
        public JsonActionResult(HttpRequestMessage request, HttpStatusCode statusCode, object result)
        {
            this.Request = request;
            this.Result = JsonConvert.SerializeObject(result);
            this.StatusCode = statusCode;
        }

        /// <summary>
        /// Inicializa el codigo y el mensaje de la transacción
        /// </summary>
        /// <param name="request">HttpRequestMessage del ApiController actual</param>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="StatusDescription">Descripcion de transacción</param>
        public JsonActionResult(HttpRequestMessage request, HttpStatusCode statusCode, string statusDescription)
        {
            this.Request = request;
            this.StatusDescription = statusDescription;
            this.StatusCode = statusCode;
        }

        /// <summary>
        /// Inicializa el codigo, el mensaje y el objeto resultado de la transacción
        /// </summary>
        /// <param name="request">HttpRequestMessage del ApiController actual</param>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="StatusDescription">Descripcion de transacción</param>
        /// <param name="Result">Objeto resultado de la transacción</param>
        public JsonActionResult(HttpRequestMessage request, HttpStatusCode statusCode, string statusDescription,  object result)
        {
            this.Request = request;
            this.Result = JsonConvert.SerializeObject(result);
            this.StatusDescription = statusDescription;
            this.StatusCode = statusCode;
        }

        /// <summary>
        /// Ejecuta la peticion de solicitud de manera asincrona
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            return Task.FromResult(Execute());
        }

        /// <summary>
        /// Ejecuta la peticion de solicitud y convierte el resultado en un objeto json
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Execute()
        {
            var response = this.Request.CreateResponse(StatusCode);
            response.ReasonPhrase = StatusDescription;
            if (Result != null)
                response.Content = new StringContent(Result, Encoding.UTF8, "application/json");
            
            return response;
        }
    }
}