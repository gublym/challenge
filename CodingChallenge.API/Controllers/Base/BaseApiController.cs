﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Helpers;
using Newtonsoft.Json;
using System.Net;
using System.Web.Http.Cors;

namespace CodingChallenge.API.Controllers.Base
{
    /// <summary>
    /// Clase base para todos los controllers de tipo API
    /// </summary>
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public abstract class BaseApiController : ApiController
    {
        #region JsonResult methods

        /// <summary>
        /// Devuelve una respuesta OK a la peticion
        /// </summary>
        protected internal virtual JsonActionResult JsonApiResult()
        {
            return new JsonActionResult(Request);
        }

        /// <summary>
        /// Devuelve una respuesta con el codigo indicado y el objeto resultado
        /// </summary>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="Result">Objeto resultado de la transacción</param>
        protected internal virtual JsonActionResult JsonApiResult(HttpStatusCode statusCode, object result)
        {
            return new JsonActionResult(Request, statusCode, result);
        }

        /// <summary>
        /// Devuelve una respuesta con el codigo indicado y el mensaje de la transaccion
        /// </summary>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="StatusDescription">Descripcion de transacción</param>
        protected internal virtual JsonActionResult JsonApiResult(HttpStatusCode statusCode, string statusDescription)
        {
            return new JsonActionResult(Request, statusCode, statusDescription);
        }

        /// <summary>
        /// Devuelve una respuesta con el codigo, el mensaje y el objeto resultado de la transacción
        /// </summary>
        /// <param name="statusCode">Codigo de transacción</param>
        /// <param name="StatusDescription">Descripcion de transacción</param>
        /// <param name="Result">Objeto resultado de la transacción</param>
        protected internal virtual JsonActionResult JsonApiResult(HttpStatusCode statusCode, string statusDescription, object result)
        {
            return new JsonActionResult(Request, statusCode, statusDescription, result);
        }

        #endregion
    }
}
