﻿using CodingChallenge.Entity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CodingChallenge.API.Controllers.Base
{
    /// <summary>
    /// Interface que regula los metodos generales de los controllers
    /// </summary>
    /// <typeparam name="T">Objetio de tipo Entidad</typeparam>
    public interface IBaseController<T> where T : BaseEntity
    {
        /// <summary>
        /// Servicio que obtiene todos los elementos de este objeto
        /// </summary>
        /// <returns></returns>
        JsonActionResult GetAll();

        /// <summary>
        /// Servicio que obtiene el elemento que coincida con el objeto pasado como parametro
        /// </summary>
        /// <param name="pEntity">Entidad a buscar</param>
        /// <returns></returns>
        JsonActionResult GetByEntity(T pEntity);

        /// <summary>
        /// Servicio que actualiza el objeto pasados como parametro
        /// </summary>
        /// <param name="pEntities">Entidades a actualizar</param>
        /// <returns></returns>
        JsonActionResult Update(T pEntity);

        /// <summary>
        /// Servicio que agrega el objeto pasados como parametro
        /// </summary>
        /// <param name="pEntities">Entidades a agregar</param>
        /// <returns></returns>
        JsonActionResult Add(T pEntity);

        /// <summary>
        /// Servicio que elimina el objeto pasados como parametro
        /// </summary>
        /// <param name="pEntities">Entidades a eliminar</param>
        /// <returns></returns>
        JsonActionResult Delete(T pEntity);
    }
}
