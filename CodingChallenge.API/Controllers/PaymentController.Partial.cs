﻿using CodingChallenge.API.Controllers.Base;
using CodingChallenge.API.Controllers.Token;
using CodingChallenge.Common.Exceptions;
using CodingChallenge.Entity;
using REMEDY.Business;
using REMEDY.Business.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CodingChallenge.API.Controllers
{
    public partial class PaymentController
    {
        /// <summary>
        /// Servicio que obtiene la información de todos los pagos
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public override JsonActionResult GetAll()
        {
            try
            {
                var entities = _baseBusiness.GetAll(t=> t.PaymentType, t=> t.User);
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", entities);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("PaymentController<T>", "GetAll", ex));
            }
        }
    }
}
