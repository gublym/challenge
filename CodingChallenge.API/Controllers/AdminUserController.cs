﻿using CodingChallenge.API.Controllers.Base;
using CodingChallenge.API.Controllers.Token;
using CodingChallenge.Business;
using CodingChallenge.Business.Interface;
using CodingChallenge.Common.Exceptions;
using CodingChallenge.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CodingChallenge.API.Controllers
{
    [AllowAnonymous]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AdminUserController : BaseApiController
    {
        private IAdminUserBusiness Business { get; set; }

        public AdminUserController()
        {
            this.Business = new AdminUserBusiness();
        }

        [HttpGet]
        public JsonActionResult GetAllPermissions()
        {
            try
            {
                var result = this.Business.GetAllPermissions();
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", result);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("AdminUserController", "GetAllPermissions", ex));
            }
        }

        [HttpGet]
        public JsonActionResult GetAllRoles()
        {
            try
            {
                var result = this.Business.GetAllRoles();
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", result);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("AdminUserController", "GetAllPermissions", ex));
            }
        }

        [HttpGet]
        public JsonActionResult GetAllUsers()
        {
            try
            {
                var result = this.Business.GetAllUsers();
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", result);
            }
            catch (Exception ex)
            {
                return JsonApiResult(HttpStatusCode.InternalServerError, new ServiceException("AdminUserController", "GetAllPermissions", ex));
            }
        }
    }
}
