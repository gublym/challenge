﻿using CodingChallenge.API.Controllers.Base;
using CodingChallenge.API.Controllers.Token;
using CodingChallenge.Business;
using CodingChallenge.Business.Interface;
using CodingChallenge.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace CodingChallenge.API.Controllers
{
    

    [AllowAnonymous]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : BaseApiController
    {
        private ILoginBusiness Business { get; set; }

        public LoginController()
        {
            this.Business = new LoginBusiness();
        }

        [HttpPost]
        public JsonActionResult Authenticate(User login)
        {
            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            var validUser = this.Business.ValidateLogin(login);
            
            if (validUser != null)
            {
                var token = TokenGenerator.GenerateTokenJwt(login.UserName);
                var result = new
                {
                    Id = validUser.Id,
                    UserName = validUser.UserName,
                    Token = token,
                    IdRole = validUser.IdRole
                };
                
                return JsonApiResult(HttpStatusCode.OK, "Su petición se ha realizado con éxito", result);
            }
            else
            {
                return JsonApiResult(HttpStatusCode.Unauthorized, "El email o contraseña es incorrecto");
            }
        }
    }
}
