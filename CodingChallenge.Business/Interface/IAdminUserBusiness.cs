﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CodingChallenge.Entity;

namespace CodingChallenge.Business.Interface
{
    public interface IAdminUserBusiness
    {
        IEnumerable<Permission> GetAllPermissions();
        IEnumerable<Role> GetAllRoles();
        IEnumerable<User> GetAllUsers();

    }
}