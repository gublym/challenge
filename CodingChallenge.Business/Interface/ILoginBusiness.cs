﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CodingChallenge.Entity;

namespace CodingChallenge.Business.Interface
{
    public interface ILoginBusiness
    {
        /// <summary>
        /// Validate if a user is valid
        /// </summary>
        /// <param name="pUsuario">User</param>
        /// <returns>If an user is valid</returns>
        User ValidateLogin(User pUsuario);

    }
}