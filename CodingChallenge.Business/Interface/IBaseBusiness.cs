﻿using CodingChallenge.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Business.Interface
{
    public interface IBaseBusiness<T> where T : BaseEntity
    {
        /// <summary>
        /// Obtiene todos los elementos de la lista
        /// </summary>
        /// <param name="navigationProperties">Expresión Linq</param>
        /// <returns></returns>
        IEnumerable<T> GetAll(params Expression<Func<T, object>>[] navigationProperties);


        /// <summary>
        /// Obtiene la entidad por el identificador
        /// </summary>
        /// <param name="pId">Identificador de la entidad</param>
        /// <returns></returns>
        T GetByEntity(T pEntity);

        /// <summary>
        /// Agrega una o varias entidades
        /// </summary>
        /// <param name="entidades">Entidades</param>
        bool Add(params T[] entidades);

        /// <summary>
        /// Actualiza una o varias entidades
        /// </summary>
        /// <param name="entidades">Entidades</param>
        bool Update(params T[] entidades);

        /// <summary>
        /// Elimina una o varias entidades
        /// </summary>
        /// <param name="entidades">Entidades</param>
        bool Remove(params T[] entidades);
    }
}
