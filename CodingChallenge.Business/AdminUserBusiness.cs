﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.IO;
using CodingChallenge.Entity;
using CodingChallenge.DataAccess.Interface;
using CodingChallenge.DataAccess.Repository;
using CodingChallenge.Common.Exceptions;
using CodingChallenge.Business.Interface;

namespace CodingChallenge.Business
{
    public partial class AdminUserBusiness : IAdminUserBusiness
    {
        /// <summary>
        /// User Repository
        /// </summary>
        protected IUserRepository _userRepository { get; private set; }

        /// <summary>
        /// User Repository
        /// </summary>
        protected IPermissionRepository _permissionRepository { get; private set; }

        /// <summary>
        /// User Repository
        /// </summary>
        protected IRoleRepository _roleRepository { get; private set; }


        /// <summary>
        /// Default Constuctor
        /// </summary>
        public AdminUserBusiness()
        {
            _userRepository = new UserRepository();
            _permissionRepository = new PermissionRepository();
            _roleRepository = new RoleRepository();
        }

        
        public IEnumerable<Permission> GetAllPermissions()
        {
            return this._permissionRepository.GetAll(t => t.RolesPermissions);
        }

        public IEnumerable<Role> GetAllRoles()
        {
            return this._roleRepository.GetAll(t => t.RolesPermissions);
        }

        public IEnumerable<User> GetAllUsers()
        {
            return this._userRepository.GetAll(t => t.Role);
        }

    }
}