﻿using CodingChallenge.Business.Interface;
using CodingChallenge.Common.Exceptions;
using CodingChallenge.DataAccess.Interface;
using CodingChallenge.DataAccess.Repository;
using CodingChallenge.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Business.Entity
{
    /// <summary>
    /// Representa la clase base para las operaciones básicas de negocio
    /// </summary>
    /// <typeparam name="T">Entidad</typeparam>
    public abstract class BaseBusiness<T> : IBaseBusiness<T> where T : BaseEntity
    {
        /// <summary>
        /// Repositorio de la entidad
        /// </summary>
        protected IBaseRepository<T> _baseRepository { get; private set; }

        /// <summary>
        /// Constructor por defecto
        /// </summary>
        public BaseBusiness()
        {
            _baseRepository = new BaseRepository<T>();
        }

        /// <summary>
        /// Inicializa una instancia de <see cref="BaseNegocio"/> con el repositorio dado
        /// </summary>
        /// <param name="pRepositorio">Repositorio de la entidad</param>
        public BaseBusiness(IBaseRepository<T> pRepositorio)
        {
            _baseRepository = pRepositorio;
        }

        /// <summary>
        /// Obtiene todos los elementos de la entidad
        /// </summary>
        /// <returns></returns>
        public IEnumerable<T> GetAll(params Expression<Func<T, object>>[] navigationProperties)
        {
            try
            {
                return _baseRepository.GetAll(navigationProperties);
            }
            catch (Exception ex)
            {
                throw new BusinessException("BaseBusiness<T>", "GetAll", ex);
            }
        }

        /// <summary>
        /// Obtiene la entidad por el identificador
        /// </summary>
        /// <param name="pId">Identificador de la entidad</param>
        /// <returns></returns>
        public virtual T GetByEntity(T pEntity)
        {
            try
            {
                return _baseRepository.Get(pEntity);
            }
            catch (Exception ex)
            {
                throw new BusinessException("BaseBusiness<T>", "GetByEntity", ex);
            }
        }

        /// <summary>
        /// Agrega uno o varias entidades
        /// </summary>
        /// <param name="entidades"></param>
        public bool Add(params T[] entidades)
        {
            try
            {
                return _baseRepository.Add(entidades);
            }
            catch (Exception ex)
            {
                throw new BusinessException("BaseBusiness<T>", "Add", ex);
            }
        }

        /// <summary>
        /// Actualiza uno o varias entidades
        /// </summary>
        /// <param name="entidades"></param>
        public bool Update(params T[] entidades)
        {
            try
            {
                return _baseRepository.Update(entidades);
            }
            catch (Exception ex)
            {
                throw new BusinessException("BaseBusiness<T>", "Update", ex);
            }
        }

        /// <summary>
        /// Elimina una o varias entidades
        /// </summary>
        /// <param name="entidades"></param>
        public bool Remove(params T[] entidades)
        {
            try
            {
                return _baseRepository.Remove(entidades);
            }
            catch (Exception ex)
            {
                throw new BusinessException("BaseBusiness<T>", "Remove", ex);
            }
        }
    }

}
