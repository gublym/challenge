﻿using System;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using System.Net.Mail;
using System.Net;
using System.IO;
using CodingChallenge.Entity;
using CodingChallenge.DataAccess.Interface;
using CodingChallenge.DataAccess.Repository;
using CodingChallenge.Common.Exceptions;
using CodingChallenge.Business.Interface;

namespace CodingChallenge.Business
{
    public partial class LoginBusiness : ILoginBusiness
    {
        /// <summary>
        /// User Repository
        /// </summary>
        protected IUserRepository _userRepository { get; private set; }

        /// <summary>
        /// Default Constuctor
        /// </summary>
        public LoginBusiness()
        {
            _userRepository = new UserRepository();
        }

        /// <summary>
        /// Validate if an user is valid
        /// </summary>
        /// <param name="pUsuario">User</param>
        /// <returns>If valid user</returns>
        public User ValidateLogin(User pUsuario)
        {
            User salida = null;
            try
            {
                if (pUsuario != null
                && !string.IsNullOrWhiteSpace(pUsuario.UserName)
                && !string.IsNullOrWhiteSpace(pUsuario.Password))
                {
                    var usuario = _userRepository.GetList(t => t.Password == pUsuario.Password && t.UserName == pUsuario.UserName);

                    if(usuario != null && usuario.Any() && usuario.Count() == 1)
                    {
                        salida = usuario.FirstOrDefault();
                    }
                }
            }
            catch(Exception ex)
            {
                throw new BusinessException("LoginBusiness", "ValidateLogin", ex);
            }

            return salida;
        }

    }
}