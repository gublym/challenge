import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from 'src/app/shared/services/authentication.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'challenge-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  form = this.fb.group({
    user: [null, Validators.required],
    password: [null, Validators.required],
  });

  constructor(private fb: FormBuilder,
    private authenticationService: AuthenticationService,
    private router: Router) {
      // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) {
      this.router.navigate(['/']);
    }
    }

  onSubmit() {
    // stop here if form is invalid
      if (this.form.invalid) {
      return;
    }
      this.authenticationService.login(this.form.controls.user.value, this.form.controls.password.value)
      .pipe(first())
      .subscribe(
        data => {
              this.router.navigate(['/']);
        },
        error => {
          alert("We couldn't validate your credentials!");
        });
  }
}
