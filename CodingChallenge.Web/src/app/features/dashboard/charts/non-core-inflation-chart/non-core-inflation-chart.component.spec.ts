import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NonCoreInflationChartComponent } from './non-core-inflation-chart.component';

describe('NonCoreInflationChartComponent', () => {
  let component: NonCoreInflationChartComponent;
  let fixture: ComponentFixture<NonCoreInflationChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonCoreInflationChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonCoreInflationChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
