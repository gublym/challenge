import { Component, OnInit, Input } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { BancoMexicoService } from 'src/app/shared/services/banco-mexico.service';

@Component({
  selector: 'challenge-non-core-inflation-chart',
  templateUrl: './non-core-inflation-chart.component.html',
  styleUrls: ['./non-core-inflation-chart.component.scss']
})
export class NonCoreInflationChartComponent implements OnInit {

  // Initializing variables
  public lineChartOptions: ChartOptions = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];

  @Input() lineChartData: ChartDataSets[];
  @Input() lineChartLabels: Label[];
  constructor(private service : BancoMexicoService) { }

  ngOnInit() {
    // initializing the chart
    this.lineChartData = [
      { data: [], label: '' },
    ];
    this.lineChartLabels = [];
  }

}


