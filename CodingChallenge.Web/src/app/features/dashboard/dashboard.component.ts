import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Breakpoints, BreakpointObserver } from '@angular/cdk/layout';
import { BancoMexicoService } from 'src/app/shared/services/banco-mexico.service';
import { ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import { Datos, Sumary } from 'src/app/shared/models/chart';

@Component({
  selector: 'challenge-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  /** Based on the screen size, switch from standard to one column per row */
  cardLayout = this.breakpointObserver.observe([Breakpoints.Handset, Breakpoints.Tablet]).pipe(
    map(({ matches }) => {
      if (matches) {
        return {
          columns: 1,
          miniCard: { cols: 1, rows: 1 },
          chart: { cols: 1, rows: 2 },
          table: { cols: 1, rows: 3 },
        };
      }
 
     return {
        columns: 4,
        miniCard: { cols: 1, rows: 1 },
        chart: { cols: 4, rows: 3 },
        table: { cols: 4, rows: 4 },
      };
    })
  );

  // variables for miniCards
  public miniCardData: Sumary[];

  // variables for chart
  public lineChartData: ChartDataSets[];
  public lineChartLabels: Label[];

  // table chart
  public sourceTable: Datos[];

  constructor(
    private breakpointObserver: BreakpointObserver,
    private service: BancoMexicoService
    ) {}

  ngOnInit()
  {
    let data = [];
    let dataLabel = [];
    let label = "";
    let average = 0;
    let minVal = 0;
    let thisYearValues = [];
    let pastYearValues = [];
    let actualYear =(new Date()).getFullYear();

    this.service.getNonCoreInflationData().subscribe(t=> {
      // generate de chart data
      label = t.bmx.series[0].titulo;
      t.bmx.series[0].datos.forEach(item=>{
        data.push(+item.dato);
        dataLabel.push(item.fecha);
        average = average + (+item.dato);
        // add values from this year and last
        let yearData =  new Date(item.fecha).getFullYear();
        if(actualYear == yearData)
        {
          thisYearValues.push(+item.dato);
        }
        if(actualYear - 1 == yearData)
        {
          pastYearValues.push(+item.dato);
        }
      });

      this.lineChartData= [
        { data: data, label: label },
      ];

      this.lineChartLabels = dataLabel;

      // asign datasource for table
      this.sourceTable = t.bmx.series[0].datos;

      // creating the sumary data for minicards
      this.miniCardData = [];

      let mean = new Sumary();
      average = average / t.bmx.series[0].datos.length;
      mean.color ="green";
      mean.icon = ' linear_scale';
      mean.isCurrency = true;
      mean.title = "Mean (average)";
      mean.value = +average.toFixed(2);

      let lowest = new Sumary();
      minVal = Math.min.apply(Math, data);
      lowest.color ="green";
      lowest.icon = 'vertical_align_bottom';
      lowest.isCurrency = true;
      lowest.title = "Lowest (historical)";
      lowest.value = minVal;

      let minInYear = new Sumary();
      let minThisYear = Math.min.apply(Math, thisYearValues);
      let minPastYear = Math.min.apply(Math, pastYearValues);
      let minPercent = (minThisYear * 100)/minPastYear;
      minInYear.color ="red";
      minInYear.icon = 'minimize';
      minInYear.isCurrency = true;
      minInYear.title = "Min in Year";
      minInYear.value = minThisYear;
      minInYear.percentValue = minPercent;
      minInYear.isCurrency = true;
      minInYear.isIncrease = minThisYear >= minPastYear;

      let maxInYear = new Sumary();
      let maxThisYear = Math.max.apply(Math, thisYearValues);
      let maxPastYear = Math.max.apply(Math, pastYearValues);
      let maxPercent = (maxThisYear * 100)/maxPastYear;
      maxInYear.color ="green";
      maxInYear.icon = 'maximize';
      maxInYear.isCurrency = true;
      maxInYear.title = "Max in Year";
      maxInYear.value = maxThisYear;
      maxInYear.percentValue = maxPercent;
      maxInYear.isCurrency = true;
      maxInYear.isIncrease = maxThisYear >= maxPastYear;

      this.miniCardData.push(mean);
      this.miniCardData.push(lowest);
      this.miniCardData.push(minInYear);
      this.miniCardData.push(maxInYear);
    });
  }

}
