import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';

import { NonCoreInflationTableComponent } from './non-core-inflation-table.component';

describe('NonCoreInflationTableComponent', () => {
  let component: NonCoreInflationTableComponent;
  let fixture: ComponentFixture<NonCoreInflationTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NonCoreInflationTableComponent ],
      imports: [
        NoopAnimationsModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NonCoreInflationTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
