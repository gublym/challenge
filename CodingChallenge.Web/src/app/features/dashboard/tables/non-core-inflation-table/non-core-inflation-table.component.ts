import { AfterViewInit, Component, OnInit, ViewChild, Input, SimpleChanges } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { NonCoreInflationTableDataSource } from './non-core-inflation-table-datasource';
import { Datos } from 'src/app/shared/models/chart';

@Component({
  selector: 'challenge-non-core-inflation-table',
  templateUrl: './non-core-inflation-table.component.html',
  styleUrls: ['./non-core-inflation-table.component.scss']
})
export class NonCoreInflationTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatTable) table: MatTable<Datos>;
  dataSource: NonCoreInflationTableDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['fecha', 'dato'];

  @Input() data: Datos[];
  ngOnInit() {
    
  }

  // detect when new source
  ngOnChanges(changes: SimpleChanges) {
    if(typeof changes.data.currentValue !== 'undefined' && changes.data.currentValue !== null
      && changes.data.currentValue)
      {
        this.refreshTableData(changes.data.currentValue);
      }
  }

  ngAfterViewInit() {
    this.refreshTableData([]);
  }

  refreshTableData(newData: Datos[])
  {
    this.dataSource = new NonCoreInflationTableDataSource();
    this.dataSource.data = newData;
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
    this.table.dataSource = this.dataSource;
  }
}
