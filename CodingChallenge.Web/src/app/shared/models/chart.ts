export class BmxRest {
  bmx: Bmx;
}

export class Bmx {
  series: Serie[];
}

export class Serie {
  idSerie: string;
  titulo: string;
  datos: Datos[];
}

export class Datos {
  fecha: string;
  dato: string;
}

export class Sumary
{
  icon: string;
  title: string;
  value: number;
  color: string;
  isIncrease: boolean;
  isCurrency: boolean;
  duration: string;
  percentValue: number;
}