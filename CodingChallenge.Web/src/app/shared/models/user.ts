export class User {
  Id: number;
  UserName: string;
  Password: string;
  Token: string;
  IdRole: number;
  Role: Role;
}

export class Permission {
  Id: number;
  Name: string;
  Description: string;
  RolesPermissions: any[]
}

export class Role {
  Id: number;
  Name: string;
  Description: string;
  RolesPermissions: any[]
}

export class RolesPermissions {
  IdRol: number;
  IdPermission: string;
  Visible: true;
}


