import { Injectable } from '@angular/core';
import { FormControl, FormGroup, AbstractControl, ValidatorFn, Validators } from '@angular/forms';
import * as moment from 'moment';


@Injectable()
export class CustomValidatorService {

  private static minRange = 0;
  private static maxRange = 0;

  /**
 * Método para validar los campos de formGroup
 * @returns {void}
 */
  static isFieldInvalid(field: string, form: FormGroup, fsa: boolean): boolean {
    return (
      CustomValidatorService.getErrorMessage(field, form, fsa) !== ''
    );
  }

  /**
   * Método para validar los campos de formGroup
   * @returns {void}
   */
  static getErrorMessage(field: string, form: FormGroup, fsa: boolean): string {
    let message = '';
    const formField = form.controls[field];
    if ((!formField.valid && formField.touched) ||
      (formField.untouched && fsa)) {

      if (formField.hasError('required')) {
        message += 'El campo es requerido.';
      }

      else if (formField.hasError('email')) {
        message += 'El correo no es válido.';
      }

      else if (formField.hasError('invalidChar')) {
        message += 'Los carácteres especiales no están permitidos.';
      }

      else if (formField.hasError('onlyNumbers')) {
        message += 'Sólo se permiten números.';
      }

      else if (formField.hasError('invalidRFC')) {
        message += 'El RFC no es válido.';
      }

      else if (formField.hasError('invalidCURP')) {
        message += 'La CURP no es válida.';
      }

      else if (formField.hasError('matchPassword')) {
        message += 'Las contraseñas deben coincidir.';
      }

      else if (formField.hasError('existeEmail')) {
        message += 'El email proporcionado ya existe.';
      }
      
      else if (formField.hasError('existeCURP')) {
        message += 'La CURP proporcionada ya existe.';
      }
      
      else if (formField.hasError('existeCedula')) {
        message += 'La cédula proporcionada ya existe.';
      }

      else if (formField.hasError('existeTelefono')) {
        message += 'El teléfono proporcionado ya existe.';
      }

      else if (formField.hasError('selectValidOption')) {
        message += 'La opción seleccionada es inválida.';
      }

      else if (formField.hasError('selectTrueOption')) {
        message += 'La opción seleccionada es inválida.';
      }

      else if (formField.hasError('rangeChars')) {
        if(this.minRange === this.maxRange)
        {
          message += `El campo debe cumplir con ${this.minRange} caracteres.`;
        }
        else
        {
          message += `Se aceptan mínimo ${this.minRange} y máximo ${this.maxRange} caracteres.`;
        }
      }

      else if (formField.hasError('minLength')) {
        message += `Se aceptan mínimo ${this.minRange} caracteres.`;
      }

      else if (formField.hasError('maxLength')) {
        message += `Se aceptan máximo ${this.maxRange} caracteres.`;
      }

      else if (formField.hasError('underEighteen')) {
        message += `Es necesario que seas mayor de edad para registrarte.`;
      }

      else if (formField.hasError('strongPasswordDigit')) {
        message += `La contraseña debe contener al menos un dígito.`;
      }

      else if (formField.hasError('strongPasswordLowerCase')) {
        message += `La contraseña debe contener al menos una letra minúscula.`;
      }

      else if (formField.hasError('strongPasswordUpperCase')) {
        message += `La contraseña debe contener al menos una letra mayúscula.`;
      }

      else if (formField.hasError('strongPasswordLength')) {
        message += `La contraseña debe tener entre 8 y 30 caractéres.`;
      }

    }
    return message;

  }

  static selectValidOption(control: FormControl): { [key: string]: any } {
    if (control.value) {
      if (typeof control.value !== "string" && control.value !== '0') {
        return null;
      } else {
        return { 'selectValidOption': true };
      }
    }
  }

  static selectTrueOption(control: FormControl): { [key: string]: any } {
    if (control.value) {
      return null;
    }
    else
    {
      return { 'selectTrueOption': true };
    }
  }

  static overEighteen(control: FormControl): { [key: string]: any } {
    if (control.value) {
      const dob = control.value;
      const today = moment().startOf('day');
      const delta = today.diff(dob, 'years', false);

      if (delta <= 18) {
        return { 'underEighteen': true };
      }

      return null;
    }
  }

  static specialChar(control: FormControl): { [key: string]: any } {
    if (control.value) {
      if (!control.value.match(/[-!$%^&*()_+|~=`{}\[\]:";#@'<>?,.\/]/)) {
        return null;
      } else {
        return { 'invalidChar': true };
      }
    }
  }

  static onlyNumbers(control: FormControl): { [key: string]: any } {
    if (control.value) {
      if (control.value.match(/^[0-9]+$/)) {
        return null;
      } else {
        return { 'onlyNumbers': true };
      }
    }
  }

  static rfc(control: FormControl): { [key: string]: any } {
    if (control.value) {
      if (control.value.match(
        /^([A-ZÑ\x26]{3,4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([A-Z]|[0-9]){2}([A]|[0-9]){1})?$/)) {
        return null;
      } else {
        return { 'invalidRFC': true };
      }
    }
  }

  static curp(control: FormControl): { [key: string]: any } {
    if (control.value) {
      if (control.value.match(
        /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/)) {
        return null;
      } else {
        return { 'invalidCURP': true };
      }
    }
  }

  static onlyUppercase(control: FormControl): { [key: string]: any } {
    if (control.value) {
      if (control.value.match(/[A-Z0-9]/)) {
        return null;
      } else {
        return { 'onlyUppercase': true };
      }
    }
  }

  static rangeChars(prms: any): ValidatorFn {
    return (control: FormControl): { [key: string]: boolean } => {
      const val: string = control.value;
      this.minRange = prms.min;
      this.maxRange = prms.max;
      if(val !== "")
      {
        if (!isNaN(prms.min) && !isNaN(prms.max)) {
          return val.length < prms.min || val.length > prms.max ? { 'rangeChars': true } : null;
        } else if (!isNaN(prms.min)) {
  
          return val.length < prms.min ? { 'minLength': true } : null;
        } else if (!isNaN(prms.max)) {
  
          return val.length > prms.max ? { 'maxLength': true } : null;
        }
      }
      return null;
    };
  }

  static matchPassword(firstControlName, secondControlName) {
    return (AC: AbstractControl) => {
      const firstControlValue = AC.get(firstControlName).value;
      const secondControlValue = AC.get(secondControlName).value;
      if (firstControlValue !== secondControlValue) {
        AC.get(secondControlName).setErrors({ 'matchPassword': true });
        return { 'matchPassword': true };
      } else {
        return null;
      }
    };
  }

  static strongPassword(control: FormControl): { [key: string]: any } {
    if (control.value) {
      if (!control.value.match(/^(?=\D*\d)$/)) {
        return { 'strongPasswordDigit': true };
      }else if (!control.value.match(/^(?=[^a-z]*[a-z])$/)) {
        return { 'strongPasswordLowerCase': true };
      }else if (!control.value.match(/^(?=[^A-Z]*[A-Z])$/)) {
        return { 'strongPasswordUpperCase': true };
      }else if (!control.value.match(/^.{8,30}$/)) {
        return { 'strongPasswordLength': true };
      }else {
        return null;
      }
    }
  }
}
