import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../models/user';
import { BmxRest } from '../models/chart';


@Injectable({ providedIn: 'root' })
export class BancoMexicoService {

  
  constructor(private http: HttpClient) {
  }

  
  public getNonCoreInflationData() : Observable<BmxRest>{
    const bmxtoken = "783ab36a464f98419be4758c31fb785d03ad701096e0ae6b5ccc50242367e5ac";
    let headers: HttpHeaders = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    headers = headers.append('Accept', 'application/json');
    headers = headers.append('Bmx-Token', bmxtoken);
    //headers = headers.append('Accept-Encoding', 'gzip');
    
    return this.http.get<BmxRest>("https://www.banxico.org.mx/SieAPIRest/service/v1/series/SP74665/datos", { headers: headers });
  }

  
}
