import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Permission, Role, RolesPermissions, User } from '../models/user';

@Injectable()
export class DataService {

  dataChange: BehaviorSubject<User[]> = new BehaviorSubject<User[]>([]);
  // Temporarily stores data from dialogs
  dialogData: any;

  constructor (private httpClient: HttpClient) {}

  get data(): User[] {
    return this.dataChange.value;
  }

  getDialogData() {
    return this.dialogData;
  }

  /** CRUD METHODS */
  getAllUsers(): void {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    this.httpClient.get<User[]>(environment.apiUrl+"/user/getall", { headers: headers }).subscribe(data => {
      this.dataChange.next(data);
    },
    (error: HttpErrorResponse) => {
    console.log (error.name + ' ' + error.message);
    });
  }

  addUser (user: User): void {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    this.httpClient.post(environment.apiUrl+"/user/add", user).subscribe(data => {
      this.dialogData = user;
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
    });
  }

  updateUser (user: User): void {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    this.httpClient.put(environment.apiUrl+"/user/update", user).subscribe(data => {
      this.dialogData = user;
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
    });
  }

  deleteUser (user: User): void {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    this.httpClient.post(environment.apiUrl+"/user/delete", user).subscribe(data => {
      },
      (err: HttpErrorResponse) => {
        console.log('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
    });
  }

  getAllPermissions(): Observable<Permission[]> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return  this.httpClient.get<Permission[]>(environment.apiUrl+"/permission/getall", { headers: headers });
  }

  getAllRoles(): Observable<Role[]> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return  this.httpClient.get<Role[]>(environment.apiUrl+"/role/getall", { headers: headers });
  }

  getAllRolesPermissions(): Observable<RolesPermissions[]> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return  this.httpClient.get<RolesPermissions[]>(environment.apiUrl+"/rolespermission/getall", { headers: headers });
  }
}

/* REAL LIFE CRUD Methods I've used in my projects. ToasterService uses Material Toasts for displaying messages:

    // ADD, POST METHOD
    addItem(kanbanItem: KanbanItem): void {
    this.httpClient.post(this.API_URL, kanbanItem).subscribe(data => {
      this.dialogData = kanbanItem;
      this.toasterService.showToaster('Successfully added', 3000);
      },
      (err: HttpErrorResponse) => {
      this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
    });
   }

    // UPDATE, PUT METHOD
     updateItem(kanbanItem: KanbanItem): void {
    this.httpClient.put(this.API_URL + kanbanItem.id, kanbanItem).subscribe(data => {
        this.dialogData = kanbanItem;
        this.toasterService.showToaster('Successfully edited', 3000);
      },
      (err: HttpErrorResponse) => {
        this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      }
    );
  }

  // DELETE METHOD
  deleteItem(id: number): void {
    this.httpClient.delete(this.API_URL + id).subscribe(data => {
      console.log(data['']);
        this.toasterService.showToaster('Successfully deleted', 3000);
      },
      (err: HttpErrorResponse) => {
        this.toasterService.showToaster('Error occurred. Details: ' + err.name + ' ' + err.message, 8000);
      }
    );
  }
*/




