import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/helpers/auth.guard';
import { MainLayoutComponent } from './core/layout/main-layout/main-layout.component';


const routes: Routes = [
  { 
    path: 'login', 
    loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) 
  },
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      {
        path: '', 
        loadChildren: () => import('./features/dashboard/dashboard.module').then(m => m.DashboardModule),
        pathMatch: 'full'
      },
      { 
        path: 'admin-users', 
        loadChildren: () => import('./features/admin-users/admin-users.module').then(m => m.AdminUsersModule) 
      },
    ],
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
